package bootstrap.liftweb
import net.liftweb.http._

class Boot {
  def boot {
    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))
    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    LiftRules.autoIncludeAjax = session=>false
    LiftRules.autoIncludeComet= session=>false
    
  }
}
