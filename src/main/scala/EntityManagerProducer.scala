package freetable
import javax.enterprise.inject.Produces
import javax.persistence.{EntityManager,PersistenceContext}

class EntityManagerProducer {
  @PersistenceContext
  private var em:EntityManager = _
  @Produces
  def produce = em;
}
