package freetable

import javax.ejb._
import javax.inject.Inject
import javax.ws.rs._
import net.liftweb.json.Serialization
import org.scala_libs.jpa.ScalaEntityManager

@Path("restaurants")
trait FreeTableService {
  @GET
  def query(@QueryParam("establishment") establishment:String, 
	    @QueryParam("city") city: String,
	    @QueryParam("search") @DefaultValue("") search: String): String
  @POST
  def update(json: String):String
  @DELETE
  def delete(@QueryParam("id") id: Long):String
  @GET @Path("loadFactor")
  def getLoadFactor(@QueryParam("establishment") establishment:String, @QueryParam("city") city: String): String
  @GET @Path("updateStatus")
  def updateLoadFactor(@QueryParam("pseudoid") pseudoid:String, @QueryParam("loadFactor") loadFactor:Int):String
}

@Stateless @Local(Array(classOf[FreeTableService]))
class FreeTableServiceImpl extends FreeTableService {
  @Inject
  var em: ScalaEntityManager = _
  implicit val formats = net.liftweb.json.DefaultFormats
  
  def query(establishment: String, city: String, search: String) = Serialization.write(
    em.createQuery[Restaurant]("SELECT r FROM Restaurant r WHERE r.establishment=:type AND r.city=:city" + (if(search.isEmpty) "" else " AND (r.shortName LIKE :search OR r.address LIKE :search)"))
    .setParams((("type"->establishment) :: ("city"->city) :: (if(search.isEmpty) Nil else ("search"->("%"+search+"%") :: Nil))):_*)
    .getResultList.toList)
  
  def update(json: String):String = {
    val restaurant: Restaurant = Serialization.read(json);
    if(restaurant.id==(-1L)) em.persist(restaurant) else em.merge(restaurant);
    return "";
  }
  def delete(id: Long):String = {
    em.remove(em.getReference(classOf[Restaurant],id));
    return "";
  }

  def getLoadFactor(establishment: String, city: String) = Serialization.write(
    em.createQuery[SumAndCount]("SELECT NEW freetable.SumAndCount(SUM(r.loadFactor),COUNT(r)) FROM Restaurant r WHERE r.establishment=:type AND r.city=:city AND r.loadFactor<3")
    .setParams("type" -> establishment, "city" -> city)
    .findOne
    .filter(sumAndCount => sumAndCount.sum != null && sumAndCount.count != null)
    .map(sumAndCount => (BigDecimal(sumAndCount.sum) / BigDecimal(sumAndCount.count))
      .setScale(0, BigDecimal.RoundingMode.HALF_UP).toBigInt)
    .getOrElse(BigInt(3))
  )
  def updateLoadFactor(pseudoid:String, loadFactor:Int):String = {
    em.createQuery("UPDATE Restaurant SET loadFactor=:loadFactor WHERE pseudoid=:pseudoid")
    .setParams("pseudoid"->pseudoid,"loadFactor"->loadFactor)
    .executeUpdate()
    return "";
  }

}

class SumAndCount(val sum: java.lang.Long, val count:java.lang.Long)
