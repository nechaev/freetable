package freetable

import scala.xml._
import javax.ejb._
import javax.inject.Inject
import javax.ws.rs._
import net.liftweb.json.Serialization
import org.scala_libs.jpa.ScalaEntityManager
import dispatch._

@Path("googleload")
trait GoogleLoader {
  @GET
  def load(@QueryParam("city") city:String,
	   @QueryParam("lat") lat:Double,
	   @QueryParam("lon") lon:Double,
	   @QueryParam("key") key:String): String
}

@Stateless @Local(Array(classOf[GoogleLoader]))
class GoogleLoaderImpl extends GoogleLoader {
  @EJB
  var writer: GoogleDataWriter = _

  def load(city: String, lat:Double, lon:Double, key:String):String = {
    val svc = url("https://maps.googleapis.com/maps/api/place/search/xml?location="
		  + lat.toString + "," + lon.toString
		  + "&radius=500&types=restaurant&sensor=false&key=" + key)
    for(response <- Http(svc OK as.xml.Elem)) writer.write(city, response)
    "OK"
  }

}

trait GoogleDataWriter {
  def write(city:String, data: NodeSeq)
}

@Stateless @Local(Array(classOf[GoogleDataWriter]))
class GoogleDataWriterImpl {
  @Inject
  var em: ScalaEntityManager = _

  //@TransactionAttribute(TransactionAttributeType.REQUIRED)
  def write(city: String, data: NodeSeq) {
     for(result <- data \ "result") {
	em.persist(Restaurant(
	  (result \ "id")(0).text, //id
	  "", //pseudoid
	  (result \ "name")(0).text, //shortName
	  new java.lang.Double((result \\ "lat")(0).text),
	  new java.lang.Double((result \\ "lng")(0).text),
	  city, //city
	  (result \ "vicinity")(0).text, //address
	  "medium",
	  List("euro"),
	  "Imported from Google Places",
	  (result \ "rating").map((node:Node)=>((BigDecimal(node.text)*10).toInt)).foldLeft(0)((a,v)=>v),
	  "Restaurant",
	  3
	));
      }
   }
}
