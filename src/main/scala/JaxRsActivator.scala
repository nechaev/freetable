package freetable;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author sgl
 */
@ApplicationPath("/services")
class JaxRsActivator extends Application
