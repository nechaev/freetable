package freetable

import javax.persistence._
import scala.reflect.BeanProperty
import scala.annotation.target.beanGetter
import scala.collection.JavaConversions._

@Entity @Table(name="RESTAURANTS")
case class Restaurant(
  @BeanProperty @(Id @beanGetter) @(Column @beanGetter)(length=50)
  var id: String,
  @BeanProperty
  var pseudoid: String,
  @BeanProperty
  var shortName: String,
  @BeanProperty
  var lat: Double,
  @BeanProperty
  var lon: Double,
  @BeanProperty
  var city: String,
  @BeanProperty
  var address: String,
  @BeanProperty
  var prices: String,

  var kitchen: List[String],
  @BeanProperty
  var description: String,
  @BeanProperty
  var rating: Int,
  @BeanProperty
  var establishment: String,
  @BeanProperty
  var loadFactor: Int
) {
  def this() = this(
    null,
    null,
    null,
    0.0,
    0.0,
    null,
    null,
    null,
    Nil,
    null,
    0,
    null,
    0
  )

  @ElementCollection
  def getKitchenColumn:java.util.List[String] = new java.util.ArrayList(kitchen)
  def setKitchenColumn(value: java.util.List[String]) {
    kitchen = value.toList
  }
}
