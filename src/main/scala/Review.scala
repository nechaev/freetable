package freetable

import javax.persistence._
import scala.reflect.BeanProperty
import scala.annotation.target.beanGetter

@Entity
class Review(
  @BeanProperty @(Id @beanGetter) @(GeneratedValue @beanGetter)
  var id: Int,
  @BeanProperty @(ManyToOne @beanGetter)
  var establishment: Restaurant,
  @BeanProperty
  var author: String,
  @BeanProperty @(Lob @beanGetter)
  var text: String) {
  def this() = this(-1,null,null,null)
}
