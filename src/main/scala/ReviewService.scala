package freetable

import javax.ejb._
import javax.inject.Inject
import javax.ws.rs._
import net.liftweb.json.Serialization
import org.scala_libs.jpa.ScalaEntityManager

@Path("reviews")
trait ReviewService {
  @GET @Path("count")
  def getReviewCount(@QueryParam("establishment") establishmentId: String): String
  @GET
  def getReviews(@QueryParam("establishment") establishmentId: String):String
}

@Stateless @Local(Array(classOf[ReviewService]))
class ReviewServiceImpl extends ReviewService {
  @Inject
  var em: ScalaEntityManager = _
  implicit val formats = net.liftweb.json.DefaultFormats

  def getReviewCount(establishmentId: String) = Serialization.write(
    em.createQuery[Int]("SELECT COUNT(r) FROM Review r WHERE r.establishment.id = :establishmentId")
    .setParams("establishmentId" -> establishmentId).findOne)
  def getReviews(establishmentId: String) = Serialization.write(
    em.createQuery[Review]("SELECT r FROM Review r WHERE r.establishment.id = :establishmentId")
    .setParams("establishmentId" -> establishmentId).getResultList.toList)
}
