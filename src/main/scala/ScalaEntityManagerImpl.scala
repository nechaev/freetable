package freetable
import javax.inject.Inject
import javax.enterprise.inject.Produces
import javax.persistence.EntityManager
import org.scala_libs.jpa.{ScalaEntityManager,ScalaEMFactory}

class ScalaEntityManagerImpl(val em: EntityManager) extends ScalaEntityManager {
  val factory: ScalaEMFactory = null
}

class EMProducer {
  @Produces def produce(em: EntityManager): ScalaEntityManager = new ScalaEntityManagerImpl(em)
}
