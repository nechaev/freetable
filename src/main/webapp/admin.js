$(function() {
    var fields = ['id','pseudoid','shortName','lat','lon','city','prices','description','rating'];
    $('input[type="submit"]').on('click', function() {
	var model = {};
	for(var i=0; i<fields.length; i++) model[fields[i]] = $('#'+fields[i]).val();
	model.kitchen = $('#kitchen').val().split(',');
	$.ajax({
	    url: 'services/restaurants',
	    method: 'post',
	    data: model,
	    success: function() {
		alert('Success!');
	    }
	});
    });
});