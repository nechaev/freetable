$(function() {
    var appRoot='/freetable';
    var eventBus = _.clone(Backbone.Events);

    var filterFields = {
	'Restaurant': ['kitchen','prices'],
	'Bank': [],
	'BeautySalon': ['prices']
    }

    var favorites = {
	has: function(id) {
	    return _.indexOf(this.cache,id)>-1;
	},
	add: function(id) {
	    if(_.indexOf(this.cache,id)==-1) {
		this.cache.push(id);
		localStorage.favorites = JSON.stringify(this.cache);
	    }
	},
	remove: function(id) {
	    if(_.indexOf(this.cache,id)>-1) {
		this.cache = _.without(this.cache, id);
		localStorage.favorites = JSON.stringify(this.cache);
	    }
	}
    };
    try {
	favorites.cache = JSON.parse(localStorage.favorites);
    } catch(err) {
	favorites.cache = [];
    };

    var searchResultModel = Backbone.Model.extend({
	parse: function(props) {
	    props.favorite = favorites.has(props.id);
	    return props;
	},
	initialize: function() {
	    this.on('change:favorite', function(model, value) {
		if(value) favorites.add(model.id);
		else favorites.remove(model.id);
	    });
	},
    });

    var searchResultCollection = new (Backbone.Collection.extend({
	initialize: function() {
	    eventBus.on('updateQuery', function(query) {
		_.extend(this.query, query);
		this.fetch();
	    }, this);
	    eventBus.on('updateFilter', this.setFilter, this);
	},
	model: searchResultModel,
	url: appRoot + '/services/restaurants',
	query: {},
	fetch: function(options) {
	    Backbone.Collection.prototype.fetch.call(this, _.extend(options || {}, {data: this.query}));
	},
	filters: {},
	isVisible: function(model) {
	    for(field in this.filters) {
		var filterVal = this.filters[field];
		if(filterVal) {
		    if(_.isFunction(filterVal)) {
			if(!filterVal(model)) return false;
		    } else {
			var modelVal = model.get(field);
			if(_.isArray(filterVal)) {
			    if(_.indexOf(filterVal,modelVal)==-1) return false;
			} else if(_.isArray(modelVal)) {
			    if(_.indexOf(modelVal,filterVal)==-1) return false;
			} else if(modelVal!=filterVal) return false;
		    }
		}
	    }
	    return true;
	},
	setFilter: function(field, value) {
	    this.filters[field]=value;
	    this.doFilter();
	},
	doFilter: function() {
	    this.each(function(model) {
		model.set('visible', this.isVisible(model));
	    }, this);
	    eventBus.trigger('updateMapMarkers');
	}
    }));

    //build clustered markers on visible map area
    eventBus.on('updateMarkers', function(minLat, minLon, maxLat, maxLon, groupSize) {
	var visibleModels = searchResultCollection.filter(function(model) {
	    var lat = model.get('lat');
	    var lon = model.get('lon');
	    return model.get('visible')&&lat<maxLat&&lon<maxLon&&lat>minLat&&lon>minLon;
	});
	var threshold = groupSize*groupSize;
	var groupedModels = [];
	for(var i=0;i<visibleModels.length;i++) {
	    var model = visibleModels[i];
	    var cosLat = Math.cos(model.get('lat')*Math.PI/180);
	    var neighbors = [];
	    for(j=0;j<visibleModels.length;j++) {
		if(j!=i) {
		    var otherModel = visibleModels[j];
		    var dlat = model.get('lat')-otherModel.get('lat');
		    var dlon = (model.get('lon')-otherModel.get('lon'))*cosLat;
		    var d2 = dlon*dlon + dlat*dlat;
		    if(d2<threshold) neighbors.push({distance:d2,model:otherModel});
		}
	    }
	    var sortedNeighbors = _.sortBy(neighbors, 'distance'); //function(element) {return -element.distance;});
	    groupedModels.push({model:model, neighbors: sortedNeighbors});
	}
	var sortedModels = _.sortBy(groupedModels, function(element) {return -element.neighbors.length;});
	var markers = _.reduce(sortedModels, function(acc, modelNeighbors) {
	    if(!_.has(acc.map, modelNeighbors.model.id)) {
		var group = _.chain(modelNeighbors.neighbors)
		    .filter(function(neighbor) {return !_.has(acc.map, neighbor.model.id);})
		    .pluck('model')
		    .value();
		group.unshift(modelNeighbors.model);
		var marker = {
		    lat: modelNeighbors.model.get('lat'),
		    lon: modelNeighbors.model.get('lon'),
		    models: group
		};
		acc.markers.push(marker);
		_.each(group, function(groupModel) {
		    acc.map[groupModel.id] = marker;
		});
	    }
	    return acc;
	}, {map: {}, markers:[]});

	eventBus.trigger('drawMarkers', markers.markers);
    });

    //UI Controllers
    var backgroundStyles = ['free','moderate','busy'];
    var setLoadFactorStyle = function($el, loadFactor) {
	$el.removeClass(backgroundStyles.join(' ')).addClass(backgroundStyles[loadFactor]);
    };

    var controllers = {
	Search: Backbone.View.extend({
	    initialize: function() {
		eventBus.on('updateQuery', function(query) {
		    if(query.search) this.$('input').val(query.search);
		}, this);
	    },
	    events: {
		'click .searchBtn': function() {
		    eventBus.trigger('updateQuery',{search:this.$('input').val()});
		}
	    }
	}),
	SearchResults: Backbone.View.extend({
	    initialize: function() {
 		searchResultCollection.on('reset',_.bind(this.onLoad, this));
	    },
	    onLoad: function() {
		this.$el
		    .hide()
		    .empty()
		    .append(
			searchResultCollection.map(function(model) {
			    return new controllers.SearchResult({model: model}).render().el;
			})
		    ).show();
		_.defer(function() {
		    searchResultCollection.doFilter();
		});
	    },
	    events: {
		newSearchResults: function(event, data) {
		    $('.favorites').empty();
		    this.$('*').trigger('doFilter');
		}
	    }
	}),
	SearchResult: Backbone.View.extend({
	    tagName: 'div',
	    className: 'search-result',
	    initialize: function(options) {
		this.model.on('change:visible', function() {
		    if(this.model.get('visible')) this.$el.show();
		    else this.$el.hide();
		}, this);
	    },
	    template: _.template('<table style="width:100%;"><tr><td rowspan="2" style="width:32px;"><div class="load-light"></div><td class="list-name"><%=shortName%></td><td rowspan="2" style="width:32px;"><div class="favorite-star"></div></td></tr><tr><td class="list-address"><%=address%></td></tr></table>'),
	    render: function() {
		this.$el.html(this.template(this.model.toJSON())).hide();
		this.updateFavorite();
		setLoadFactorStyle(this.$('.load-light'), this.model.get('loadFactor'));
		return this;
	    },
	    updateFavorite: function() {
		var btn = this.$('.favorite-star');
		if(this.model.get('favorite')) btn.addClass('is-favorite');
		else btn.removeClass('is-favorite');
	    },
	    events: {
		click: function() {
		    eventBus.trigger('setMapCenter', this.model.get('lat'), this.model.get('lon'));
		    eventBus.trigger('showPopup', this.model.get('lat'), this.model.get('lon'), [this.model]);
		    eventBus.trigger('showDetails', this.model);
		},
		'click .favorite-star':function(event) {
		    event.stopPropagation();
		    this.model.set('favorite', !(this.model.get('favorite')));
		    this.updateFavorite();
		}
	    }
	}),
	Map: Backbone.View.extend({
	    favorites: localStorage.favorites || [],
	    initialize: function() {
		this.buildMap();
		this.bindEvents();
	    },
	    buildMap: function() {
		this.$map = this.$('#map-container');
		this.updateMapSize();
		$(window).on('resize', _.bind(this.updateMapSize, this));
		this.displayProjection = new OpenLayers.Projection('EPSG:4326');
		this.mapProjection = new OpenLayers.Projection('EPSG:900913');
		this.map = new OpenLayers.Map({
		    div: 'map-container',
		    projection: this.mapProjection,
		    displayProjection: this.displayProjection,
		    maxExtent: new OpenLayers.Bounds(-20037508.3428, -15496570.7397, 20037508.34, 18764656.2314),
		    units: 'm'
		});
		this.layers = {
		    osm: new OpenLayers.Layer.OSM("OpenStreetMap"),
		    google: new OpenLayers.Layer.Google("Google"),
		    bing: new OpenLayers.Layer.Bing({
			name: 'Road',
			key: 'AqTGBsziZHIJYYxgivLBf0hVdrAk9mWO5cQcb8Yux8sW5M8c8opEC2lZqKR1ZZXf',
			type: 'Road',
			culture: 'ru'
		    }),
		    markers: new OpenLayers.Layer.Markers("Markers")
		};
		this.map.addLayers(_.values(this.layers));
		this.map.zoomTo(1);
		this.map.events.register('moveend', searchResultCollection, searchResultCollection.doFilter);
	    },
	    updateMapSize: function() {
		this.$map.height($('body').innerHeight());//this.$el.innerHeight());
	    },
	    getPosition: function(lat,lon) {
		return new OpenLayers.LonLat(lon,lat).transform(this.displayProjection,this.mapProjection);
	    },
	    clearMarkers: function() {
		var ml = this.layers.markers;
		while(ml.markers.length>0) {
		    var marker = ml.markers[0];
		    ml.removeMarker(marker);
		    if(marker.popup) {
			this.map.removePopup(marker.popup);
			marker.popup.destroy();
		    }
		    marker.destroy();
		}
		searchResultCollection.each(function(model) {
		    if(model.group) {
			if(model.group.popup) model.group.popup.hide();
			delete model.group;
		    }
		});
	    },
	    groupThreshold: 32, //grouping size in pixels
	    updateMarkers: function() {
		if(this.supressUpdate) return;
		var extent = this.map.getExtent();
		if(extent.left&&extent.right&&extent.top&&extent.bottom) {
		    extent.transform(this.mapProjection, this.displayProjection);
		    var resolution = (extent.right - extent.left) / $(this.map.getViewport()).width(); // degrees per pixel
		    var groupSize = this.groupThreshold * resolution;
		    eventBus.trigger('updateMarkers', extent.bottom, extent.left, extent.top, extent.right, groupSize);
		}
	    },
	    getMarkerIcon: function(model) {
		var markers = ["green-marker.png","yellow-marker.png","red-marker.png","gray-marker.png"];
		return "img/" + markers[model.get('loadFactor')];
	    },
	    popupTemplate: _.template('<table class="popup-list-item" data-itemid="<%=id%>"><tr><td rowspan="2" style="width:32px;"><div class="load-light"></div></td><td><div class="list-name"><%=shortName%></div></td></tr><tr><td><div class="list-address"><%=address%></div></td></tr></table>'),
	    buildGroupMarker: function(groupMarker) {
		groupMarker.showPopup = function() {
		    eventBus.trigger('showPopup', this.lat, this.lon, this.models);
		};
		var models = groupMarker.models.slice(0,6); //6 markers in a circle
		var startAngle = -30*(models.length-1);
		_.each(models, function(model, index) { 
		    model.group = groupMarker;
		    var marker = new OpenLayers.Marker(
			this.getPosition(groupMarker.lat, groupMarker.lon),
			new OpenLayers.Icon(
			    this.getMarkerIcon(model),
			    new OpenLayers.Size(48,48),
			    new OpenLayers.Pixel(-24,-24)
			)
		    );
		    marker.group = groupMarker;
		    marker.events.register('mousedown', groupMarker, groupMarker.showPopup);
		    this.layers.markers.addMarker(marker);

		    var angle = startAngle+(60*index);
		    var transform = 'rotate('+angle+'deg)';
		    $(marker.icon.imageDiv).find('img').css({
			'transform': transform,
			'MozTransform': transform,
			'WebkitTransform': transform,
			'MsTransform': transform
		    });
		}, this);
	    },
	    drawMarkers: function(markers) {
		this.clearMarkers();
		_.each(markers, this.buildGroupMarker, this);
	    },
	    showPopup: function(lat, lon, models) {
		var lonlat = this.getPosition(lat, lon);
		var tpl = this.popupTemplate;
		var displayModels = models.slice(0,6);
		var content = _.reduce(displayModels, function(acc, model) {
		    return acc + tpl(model.toJSON());
		}, "") + (models.length>6 ? (" и еще " + (models.length-6) + " заведений...") : "");
		if(this.popup) {
		    this.popup.hide();
		    this.popup.setContentHTML(content);
		    this.popup.lonlat = lonlat;
		    this.popup.updatePosition();
		} else {
		    this.popup = new OpenLayers.Popup.FramedCloud(
			null,
			lonlat,
			new OpenLayers.Size(200,200),
			content,
			undefined, //this.icon,
			true
		    );
		    this.map.addPopup(this.popup);
		    $(this.popup.contentDiv).on('click', function(event) {
			var popupListItem = $(event.target).parents('.popup-list-item');
			if(popupListItem.length>0) eventBus.trigger('showDetails', searchResultCollection.get(popupListItem.attr('data-itemid')));
		    });
		}
		_.each(displayModels, function(model) {
		    setLoadFactorStyle($(this.popup.contentDiv).find('table[data-itemid="'+model.id+'"] .load-light'), model.get('loadFactor'));
		}, this);
		this.popup.show();
	    },
	    hidePopup: function() {
		if(this.popup) this.popup.hide();
	    },
	    samePlaceThreshold: 10.0, //meters
	    fitResults: function(lat,lon) {
		var samePlaceThresholdLonLat = this.samePlaceThreshold * 180 / (Math.PI * 6378100);
		samePlaceThresholdLonLat = samePlaceThresholdLonLat * samePlaceThresholdLonLat;
		var extent = this.map.getExtent();
		extent.transform(this.mapProjection, this.displayProjection);
		var viewportWidth = $(this.map.getViewport()).width();
		var resolution = (extent.right - extent.left) / viewportWidth;
		var groupSize = this.groupThreshold * resolution;
		var d2group = groupSize*groupSize;
		var center = this.map.getCenter();
		center.transform(this.mapProjection, this.displayProjection);
		var cosLat = Math.cos(center.lat*Math.PI/180);
		var visibleModels = searchResultCollection.filter(searchResultCollection.isVisible, searchResultCollection);
		var minD2 = 99999.9;
		for(var i=0;i<visibleModels.length-1;i++) {
		    var model = visibleModels[i];
		    for(j=i+1;j<visibleModels.length;j++) {
			var otherModel = visibleModels[j];
			var dlat = model.get('lat')-otherModel.get('lat');
			var dlon = (model.get('lon')-otherModel.get('lon'))*cosLat;
			var d2 = dlon*dlon + dlat*dlat;
			if(d2>samePlaceThresholdLonLat && minD2>d2) minD2 = d2;
		    }
		}
		var viewRadius = viewportWidth * Math.sqrt(minD2) / (5 * this.groupThreshold);
		var newExtent = new OpenLayers.Bounds(
		    center.lon - (viewRadius/cosLat),
		    center.lat - viewRadius,
		    center.lon + (viewRadius/cosLat),
		    center.lat + viewRadius
		);
		newExtent.transform(this.displayProjection, this.mapProjection);
		this.map.zoomToExtent(newExtent);
	    },
	    
	    bindEvents: function() {
		eventBus.on('setMapLayer', function(layer) {
		    this.map.setBaseLayer(this.layers[layer]);
		}, this);
		eventBus.on('setMapCenter', function(lat, lon, zoom) {
		    this.map.setCenter(this.getPosition(lat,lon), zoom);
		    this.updateMarkers();
		}, this);
		eventBus.on('setMapScale', function(scale) {
		    this.map.zoomTo(scale);
		    this.updateMarkers();
		}, this);
		eventBus.on('updateMapMarkers', this.updateMarkers, this);

		eventBus.on('drawMarkers', this.drawMarkers, this);
		eventBus.on('showPopup', this.showPopup, this);

		searchResultCollection.on('reset', function() {
		    if(this.popup) this.popup.hide();
		    this.supressUpdate = true;
		    this.fitResults();
		    this.supressUpdate = false;
		    this.updateMarkers();
		    //_.delay(_bind(this.updateMarkers, this), 2000);
		}, this);

		eventBus.on('updateQuery updateFilter', this.hidePopup, this);

		eventBus.on('filterVisible', function(enabled) {
		    var view  = this;
		    if(enabled) eventBus.trigger('updateFilter', 'map-visible', function(model) {
			var extent = view.map.getExtent();
			var position = view.getPosition(model.get('lat'), model.get('lon'));
			return extent.containsLonLat(position);
		    });
		    else eventBus.trigger('updateFilter', 'map-visible', undefined);
		}, this);

	    }
	}),
	MapSelector: Backbone.View.extend({
	    initialize: function() {
		_.defer(this.setMapLayer,this);
	    },
	    setMapLayer: function() {
		eventBus.trigger('setMapLayer', this.$('select').val());
	    },
	    events: {
		'change select': function() {
		    this.setMapLayer()
		}
	    }
	}),
	LoadIndicator: Backbone.View.extend({
	    loadFactor: 'none',
	    loadTextIds: ['load-free', 'load-moderate', 'load-busy'],
	    establishment: 'Restaurant',
	    setLoadFactor: function(loadFactor) {
		this.loadFactor = loadFactor;
		this.$('.load-text').hide();
		setLoadFactorStyle(this.$('.load-light'), loadFactor);
		this.$('#'+this.loadTextIds[loadFactor]).show();
	    },
	    initialize: function() {
		eventBus.on('updateQuery', function(query) {
		    if(query.establishment) this.establishment = query.establishment;
		    if(query.city) $.ajax({
			url: appRoot + '/services/restaurants/loadFactor',
			data: {
			    establishment: this.establishment,
			    city: query.city
			},
			dataType: 'json',
			success: function(data) {
			    this.setLoadFactor(data);
			},
			context:this
		    });
		}, this);
	    }
	}),
	CitySelector: Backbone.View.extend({
	    cities: {
		'Moscow': {
		    center: [55.75578612,37.61763334],
		    scale: 10
		},
		'SPb': {
		    center: [59.95000001,30.31666667],
		    scale: 10
		}
	    },
	    initialize: function() {
		this.$select = this.$('select');
		eventBus.on('updateQuery', function(query) {
		    this.$select.val(query.city);
		    //if(query.city) this.setCity(query.city)
		}, this);
	    },
	    //setCity: function(city) {
		//var cityParams = this.cities[city];
		//eventBus.trigger('setMapCenter', cityParams.center[0], cityParams.center[1]);
		//eventBus.trigger('setMapScale', cityParams.scale);
	    //},
	    events: {
		'change select': function() {
		    eventBus.trigger('updateQuery', {
			city:this.$select.val()
		    });
		}
	    }
	}),
	FilterSelector: Backbone.View.extend({
	    initialize: function() {
		this.field = this.$el.attr('data-field');
		this.$select = this.$('select');
		eventBus.on('updateQuery', function(query) {
		    if(query.establishment) {
			if(_.contains(filterFields[query.establishment], this.field)) this.$el.show();
			else this.$el.hide();
			this.$select.val('');
		    }
		}, this);
	    },
	    events: {
		'change select': function() {
		    var selectedValue = this.$select.val();
		    eventBus.trigger('updateFilter', this.field, selectedValue!=''?selectedValue:undefined);
		}
	    }
	}),
	LoadSelector: Backbone.View.extend({
	    events: {
		'change input': function() {
		    var selected = [];
		    this.$('input:checked').each(function() {
			selected.push(parseInt(this.value))
		    });
		    eventBus.trigger('updateFilter', 'loadFactor', selected.length>0?selected:undefined);
		}
	    }
	}),
	FilterFavorite: Backbone.View.extend({
	    enabled: false,
	    events: {
		'click': function() {
		    if(this.enabled) {
			this.$el.removeClass('is-favorite');
			this.enabled = false;
			eventBus.trigger('updateFilter', 'favorite');
		    } else {
			this.$el.addClass('is-favorite');
			this.enabled = true;
			eventBus.trigger('updateFilter', 'favorite', true);
		    }
		}
	    },
	    
	}),
	Details: Backbone.View.extend({
	    tpl: _.template('<div class="details-name"><%=shortName%></div>'
			    +'<div class="details-address">Адрес: <%=address%></div>'
			    +'<div class="details-description">Описание: <%=description%></div>'
			    +'<div class="details-rating"><%=rating%></div>'
			    +'<a class="details-reviews" href="#"></a>'),
	    initialize: function() {
		eventBus.on('showDetails', function(model) {
		    this.model = model;
		    this.$('.detail-content').html(this.tpl(model.toJSON()));
		    $.ajax({
			url: appRoot + '/services/reviews/count',
			data: {
			    establishment: model.id
			},
			dataType: 'json',
			success: function(data) {
			    this.$('.details-reviews').html('Отзывов:' + data);
			},
			context:this
		    });
		}, this);
	    },
	    events: {
		'click .details-reviews': function() {
		    eventBus.trigger('showReviews', this.model);
		}
	    }
	}),
	EstablishmentSelector: Backbone.View.extend({
	    initialize: function() {
		this.$select = this.$('select');
		eventBus.on('updateQuery', function(query) {
		    if(query.establishment) this.$select.val(query.establishment);
		}, this);
	    },
	    events: {
		'change select': function() {
		    eventBus.trigger('updateQuery', {
			establishment:this.$select.val()
		    });
		}
	    }
	}),
	RightPanel: Backbone.View.extend({
	    detailsHeight: 150,
	    initialize: function() {
		eventBus.on('showDetails', this.showDetails, this);
		eventBus.on('updateQuery', this.hideDetails, this);
		$(window).on('resize', _.bind(this.setHeight,this));
		this.setHeight();
	    },
	    showDetails: function() {
		if(!this.showDetails) {
		    this.showDetails = true;
		    this.setHeight();
		    this.$('#DetailsRow').show();
		}
	    },
	    hideDetails: function() {
		this.showDetails = false;
		this.$('#DetailsRow').hide();
		this.setHeight();
	    },
	    setHeight: function() {
		var detailsHeight = this.showDetails ? (this.$('.details').outerHeight() + 1) : 0;
		this.$('#searchResults').height($('body').innerHeight() - this.$('#search').outerHeight() - detailsHeight - 2);
	    },
	    events: {
		'click .hide-details': function() {
		    this.hideDetails();
		}
	    }
	}),
	Reviews: Backbone.View.extend({
	    tpl: _.template('<div class="review"><div class="review-author"><%=author%></div><div class="review-text"><%=text%></div></div>'),
	    initialize: function() {
		eventBus.on('showReviews', function(model) {
		    $.ajax({
			url: appRoot + '/services/reviews',
			data: {
			    establishment: model.id
			},
			dataType: 'json',
			success: function(data) {
			    this.$('.review-content').html(_.reduce(data, function(html, review) {
				return html + this.tpl(review);
			    }, '', this));
			    this.$el.show();
			},
			context:this
		    });
		}, this);
	    },
	    events: {
		'click .hide-reviews': function() {
		    this.$el.hide();
		}
	    }
	}),
	OnlyVisibleSwitch: Backbone.View.extend({
	    events: {
		'change input': function() {
		    eventBus.trigger('filterVisible', this.$('input:checkbox:checked').val()=='YES');
		}
	    }
	})
    }
    $('[data-controller]').each(function() {
	if(!this.controller) this.controller = new controllers[$(this).attr('data-controller')]({el: this});
    });
    navigator.geolocation.getCurrentPosition(function(position) {
	eventBus.trigger('setMapCenter', position.coords.latitude, position.coords.longitude, 10);
	eventBus.trigger('updateQuery', {
	    city: 'Moscow',
	    establishment: 'Restaurant',
	    search: ''
	});
    });
//    setInterval(function() {
//	eventBus.trigger('updateQuery',{});
//    }, 60000);
});